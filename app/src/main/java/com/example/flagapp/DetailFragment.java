package com.example.flagapp;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.flagapp.data.Country;

import org.w3c.dom.Text;

import static com.example.flagapp.data.Country.countries;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        TextView nom;
        ImageView flag;
        TextView capital;
        TextView langue;
        TextView monnaie;
        TextView population;
        TextView superficie;

        nom = view.findViewById(R.id.countryName);
        nom.setText(countries[args.getCountryId()].getName());


        capital = view.findViewById(R.id.capitalName);
        capital.setText(countries[args.getCountryId()].getCapital());


        langue = view.findViewById(R.id.language);
        langue.setText(countries[args.getCountryId()].getLanguage());

        monnaie = view.findViewById(R.id.currency);
        monnaie.setText(countries[args.getCountryId()].getCurrency());

        population = view.findViewById(R.id.population);
        population.setText(String.valueOf(countries[args.getCountryId()].getPopulation()));


        superficie = view.findViewById(R.id.area);
        superficie.setText(String.valueOf(countries[args.getCountryId()].getArea()));

        String uri = countries[args.getCountryId()].getImgUri();
        flag = view.findViewById(R.id.imageView);
        Context c = flag.getContext();
        flag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier (uri, null , c.getPackageName())));


        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}